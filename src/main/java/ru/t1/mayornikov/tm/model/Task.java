package ru.t1.mayornikov.tm.model;

import ru.t1.mayornikov.tm.api.model.IWBS;
import ru.t1.mayornikov.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

public final class Task extends AbstractUserOwnedModel implements IWBS {

    private String name = "";

    private String description = "";

    private Status status = Status.NOT_STARTED;

    private Date created = new Date();

    private String projectId;

    public Task(String name, String description, Status status) {
        if (name != null) this.name = name;
        if (description != null) this.description = description;
        if (status != null) this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getProjectId() {
        return projectId;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date date) {
        this.created = date;
    }

    @Override
    public String toString() {
        return name + " : " + description;
    }

}