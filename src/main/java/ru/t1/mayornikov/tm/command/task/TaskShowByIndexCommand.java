package ru.t1.mayornikov.tm.command.task;

import ru.t1.mayornikov.tm.model.Task;
import ru.t1.mayornikov.tm.util.TerminalUtil;

public class TaskShowByIndexCommand extends AbstractTaskCommand{

    private final static String NAME = "task-show-by-index";

    private final static String DESCRIPTION = "Show task by index.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final String userId = getUserId();
        final Task task = getTaskService().findOne(userId, TerminalUtil.nextNumber() - 1);
        getTaskService().showTask(task);
    }

}