package ru.t1.mayornikov.tm.command.task;

import ru.t1.mayornikov.tm.enumerated.Status;
import ru.t1.mayornikov.tm.util.TerminalUtil;

public class TaskStatusStartByIdCommand extends AbstractTaskCommand{

    private final static String NAME = "task-start-by-id";

    private final static String DESCRIPTION = "Set task status to started by id.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[START TASK]");
        System.out.println("ENTER TASK ID:");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        getTaskService().changeTaskStatus(getTaskService().findOne(userId, id), Status.IN_PROGRESS);
    }

}