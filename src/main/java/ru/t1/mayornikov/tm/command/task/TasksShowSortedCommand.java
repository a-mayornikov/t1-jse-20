package ru.t1.mayornikov.tm.command.task;

import ru.t1.mayornikov.tm.enumerated.Sort;
import ru.t1.mayornikov.tm.util.TerminalUtil;

import java.util.Arrays;

public class TasksShowSortedCommand extends AbstractTaskCommand{

    private final static String NAME = "task-list-sort";

    private final static String DESCRIPTION = "Show tasks with sort.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SORTED TASKS]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final Sort sort = Sort.toSort(TerminalUtil.nextLine());
        final String userId = getUserId();
        getTaskService().renderTasks(getTaskService().findAll(userId, sort));
    }

}