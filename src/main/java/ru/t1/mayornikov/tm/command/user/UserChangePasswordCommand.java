package ru.t1.mayornikov.tm.command.user;

import ru.t1.mayornikov.tm.enumerated.Role;
import ru.t1.mayornikov.tm.util.TerminalUtil;

public class UserChangePasswordCommand extends AbstractUserCommand {

    private final static String NAME = "user-change-password";

    private final static String DESCRIPTION = "Change password for current user.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE USER PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        final String password = TerminalUtil.nextLine();
        getUserService().setPassword(getAuthService().getUserId(), password);
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}