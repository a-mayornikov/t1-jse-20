package ru.t1.mayornikov.tm.command.project;

import ru.t1.mayornikov.tm.enumerated.Status;
import ru.t1.mayornikov.tm.model.Project;
import ru.t1.mayornikov.tm.util.TerminalUtil;

import java.util.Arrays;

public class ProjectStatusChangeByIndexCommand extends AbstractProjectCommand{

    private static final String NAME = "project-status-change-by-index";

    private static final String DESCRIPTION = "Set status project by index.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PROJECT STATUS]");
        System.out.println("ENTER PROJECT INDEX:");
        final String userId = getUserId();
        final Project project = getProjectService().findOne(userId, TerminalUtil.nextNumber() - 1);
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        getProjectService().changeProjectStatus(project, status);
    }

}