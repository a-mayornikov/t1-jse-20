package ru.t1.mayornikov.tm.command.system;

import ru.t1.mayornikov.tm.api.service.IProjectService;
import ru.t1.mayornikov.tm.api.service.ITaskService;
import ru.t1.mayornikov.tm.api.service.IUserService;
import ru.t1.mayornikov.tm.enumerated.Role;
import ru.t1.mayornikov.tm.enumerated.Status;
import ru.t1.mayornikov.tm.model.Project;
import ru.t1.mayornikov.tm.model.Task;
import ru.t1.mayornikov.tm.model.User;

public class DemoDataInitCommand extends AbstractSystemCommand {

    protected IProjectService getProjectService() {
        return getServiceLocator().getProjectService();
    }

    protected ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    protected IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    private final static String NAME = "demo-data";

    private final static String DESCRIPTION = "Initialize demo data.";

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        final User test = getUserService().create("test", "test", "test@test.com");
        getUserService().create("user", "user", "abvgd_mail@mail.ru");
        getUserService().create("admin", "admin", Role.ADMIN);

        getProjectService().add(test.getId(), new Project("SUPER PROJECT one", "", Status.IN_PROGRESS));
        getProjectService().add(test.getId(), new Project("DUPER PROJECT two", "Like super project one but duper.", Status.NOT_STARTED));
        getProjectService().add(test.getId(), new Project("donnuiy", "This project status is COMPLETED.", Status.COMPLETED));

        getTaskService().add(test.getId(), new Task("delo", "delat", Status.IN_PROGRESS));
        getTaskService().add(test.getId(), new Task("BIG DEAL", "GREAT LIFE", Status.NOT_STARTED));
        getTaskService().add(test.getId(), new Task("order", "make model Oleg", Status.COMPLETED));
        System.out.println("[DEMO LOAD SUCCESS]");
    }

}