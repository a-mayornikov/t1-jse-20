package ru.t1.mayornikov.tm.command.project;

import ru.t1.mayornikov.tm.model.Project;

import java.util.List;

public final class ProjectsShowCommand extends AbstractProjectCommand {

    private static final String NAME = "project-list";

    private static final String DESCRIPTION = "Show all projects.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECTS]");
        final String userId = getUserId();
        final List<Project> projects = getProjectService().findAll(userId);
        getProjectService().renderProjects(projects);
    }

}
