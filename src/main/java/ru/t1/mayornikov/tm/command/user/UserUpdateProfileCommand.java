package ru.t1.mayornikov.tm.command.user;

import ru.t1.mayornikov.tm.enumerated.Role;
import ru.t1.mayornikov.tm.model.User;
import ru.t1.mayornikov.tm.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand{

    private final static String NAME = "user-profile-update";

    private final static String DESCRIPTION = "Update current user's profile.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        final String userId = getAuthService().getUserId();
        final User user = getAuthService().getUser();
        System.out.println("[UPDATE USER PROFILE]");
        System.out.println("CURRENT FIRST NAME: " + user.getFirstName());
        System.out.println("CURRENT LAST NAME: " + user.getLastName());
        System.out.println("CURRENT MIDDLE NAME: " + user.getMiddleName());
        System.out.println("ENTER NEW FIRST NAME:");
        String firstName = TerminalUtil.nextLine();
        if (firstName == null || firstName.isEmpty()) firstName = user.getFirstName();
        System.out.println("ENTER NEW LAST NAME:");
        String lastName = TerminalUtil.nextLine();
        if (lastName == null || lastName.isEmpty()) lastName = user.getLastName();
        System.out.println("ENTER NEW MIDDLE NAME:");
        String middleName = TerminalUtil.nextLine();
        if (middleName == null || middleName.isEmpty()) middleName = user.getLastName();
        getUserService().updateUser(userId, firstName, lastName, middleName);
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }
    
}