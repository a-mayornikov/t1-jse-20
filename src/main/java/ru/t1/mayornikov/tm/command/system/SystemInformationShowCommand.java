package ru.t1.mayornikov.tm.command.system;

import static ru.t1.mayornikov.tm.util.FormatUtil.formatBytes;

public class SystemInformationShowCommand extends AbstractSystemCommand{

    private final static String NAME = "system-info";

    private final static String ARGUMENT = "-s";

    private final static String DESCRIPTION = "Show information about system.";

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SYSTEM INFO]");
        final int processorCount = Runtime.getRuntime().availableProcessors();
        final long memoryTotal = Runtime.getRuntime().totalMemory();
        final long memoryMax = Runtime.getRuntime().maxMemory();
        final long memoryFree = Runtime.getRuntime().freeMemory();
        final long memoryUse = memoryTotal - memoryFree;
        System.out.println("PROCESSORS: " + processorCount);
        System.out.println("MAX MEMORY: " + formatBytes(memoryMax));
        System.out.println("TOTAL MEMORY: " + formatBytes(memoryTotal));
        System.out.println("USAGE MEMORY: " + formatBytes(memoryUse));
        System.out.println("FREE MEMORY: " + formatBytes(memoryFree));
    }

}