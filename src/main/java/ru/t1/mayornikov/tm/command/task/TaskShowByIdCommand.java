package ru.t1.mayornikov.tm.command.task;

import ru.t1.mayornikov.tm.model.Task;
import ru.t1.mayornikov.tm.util.TerminalUtil;

public class TaskShowByIdCommand extends AbstractTaskCommand{

    private final static String NAME = "task-show-by-id";

    private final static String DESCRIPTION = "Show task by id.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        final String userId = getUserId();
        final Task task = getTaskService().findOne(userId, TerminalUtil.nextLine());
        getTaskService().showTask(task);
    }

}