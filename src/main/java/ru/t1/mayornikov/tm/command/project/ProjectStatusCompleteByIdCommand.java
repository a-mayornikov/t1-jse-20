package ru.t1.mayornikov.tm.command.project;

import ru.t1.mayornikov.tm.enumerated.Status;
import ru.t1.mayornikov.tm.model.Project;
import ru.t1.mayornikov.tm.util.TerminalUtil;

public class ProjectStatusCompleteByIdCommand extends AbstractProjectCommand{

    private static final String NAME = "project-complete-by-id";

    private static final String DESCRIPTION = "Set status project to completed by id.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String userId = getUserId();
        final Project project = getProjectService().findOne(userId, TerminalUtil.nextLine());
        getProjectService().changeProjectStatus(project, Status.COMPLETED);
    }

}