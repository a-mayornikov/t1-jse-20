package ru.t1.mayornikov.tm.command.projecttask;

import ru.t1.mayornikov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.mayornikov.tm.exception.entity.TaskNotFoundException;
import ru.t1.mayornikov.tm.util.TerminalUtil;

public class TaskBindToProjectCommand extends AbstractProjectTaskCommand{

    private final static String NAME = "task-bind-to-project";

    private final static String DESCRIPTION = "Bind task to project.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        final String userId = getUserId();
        if (!getProjectService().existsById(userId, projectId)) throw new ProjectNotFoundException();
        System.out.println("ENTER TASK ID:");
        final String taskId = TerminalUtil.nextLine();
        if (!getTaskService().existsById(userId, taskId)) throw new TaskNotFoundException();
        getProjectTaskService().bindTaskToProject(projectId, taskId);
    }

}