package ru.t1.mayornikov.tm.command.task;

import ru.t1.mayornikov.tm.enumerated.Status;
import ru.t1.mayornikov.tm.util.TerminalUtil;

import java.util.Arrays;

public class TaskStatusChangeByIdCommand extends AbstractTaskCommand{

    private final static String NAME = "task-status-change-by-id";

    private final static String DESCRIPTION = "Set task status by id.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE TASK STATUS]");
        System.out.println("ENTER TASK ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final Status status = Status.toStatus(TerminalUtil.nextLine());
        final String userId = getUserId();
        getTaskService().changeTaskStatus(getTaskService().findOne(userId, id), status);
    }

}