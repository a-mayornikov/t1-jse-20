package ru.t1.mayornikov.tm.command.task;

import ru.t1.mayornikov.tm.api.service.IProjectService;
import ru.t1.mayornikov.tm.api.service.ITaskService;
import ru.t1.mayornikov.tm.command.AbstractCommand;
import ru.t1.mayornikov.tm.enumerated.Role;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    protected IProjectService getProjectService() {
        return getServiceLocator().getProjectService();
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}