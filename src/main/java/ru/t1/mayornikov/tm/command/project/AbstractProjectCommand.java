package ru.t1.mayornikov.tm.command.project;

import ru.t1.mayornikov.tm.api.service.IProjectService;
import ru.t1.mayornikov.tm.api.service.IProjectTaskService;
import ru.t1.mayornikov.tm.command.AbstractCommand;
import ru.t1.mayornikov.tm.enumerated.Role;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected IProjectService getProjectService() {
        return getServiceLocator().getProjectService();
    }

    protected IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}