package ru.t1.mayornikov.tm.command.system;

public class ApplicationInformationShowCommand extends AbstractSystemCommand{

    private final static String NAME = "info";

    private final static String ARGUMENT = "-i";

    private final static String DESCRIPTION = "Show information about application.";

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Alexey Mayornikov");
        System.out.println("E-mail: amayornikov@t1-consulting.ru");
        System.out.println("[VERSION]");
        System.out.println("1.20.1");
    }

}