package ru.t1.mayornikov.tm.command.task;

import ru.t1.mayornikov.tm.model.Task;
import ru.t1.mayornikov.tm.util.TerminalUtil;

public class TaskUpdateByIndexCommand extends AbstractTaskCommand{

    private final static String NAME = "task-update-by-index";

    private final static String DESCRIPTION = "Update task by index.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Task task = getTaskService().findOne(getUserId(), TerminalUtil.nextNumber() - 1);
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER TO DO:");
        final String description = TerminalUtil.nextLine();
        task.setName(name);
        task.setDescription(description);
    }

}