package ru.t1.mayornikov.tm.command.project;

import ru.t1.mayornikov.tm.enumerated.Sort;
import ru.t1.mayornikov.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectsShowSortedCommand extends AbstractProjectCommand {

    private static final String NAME = "project-list-sort";

    private static final String DESCRIPTION = "Show all projects with sort.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SORTED PROJECTS]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final Sort sort = Sort.toSort(TerminalUtil.nextLine());
        final String userId = getUserId();
        getProjectService().renderProjects(getProjectService().findAll(userId, sort));
    }

}
