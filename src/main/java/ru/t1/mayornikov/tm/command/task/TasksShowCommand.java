package ru.t1.mayornikov.tm.command.task;

import ru.t1.mayornikov.tm.model.Task;

import java.util.List;

public class TasksShowCommand extends AbstractTaskCommand{

    private final static String NAME = "task-list";

    private final static String DESCRIPTION = "Show tasks.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASKS]");
        final String userId = getUserId();
        final List<Task> tasks = getTaskService().findAll(userId);
        getTaskService().renderTasks(tasks);
    }

}