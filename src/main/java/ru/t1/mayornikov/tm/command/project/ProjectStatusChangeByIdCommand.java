package ru.t1.mayornikov.tm.command.project;

import ru.t1.mayornikov.tm.enumerated.Status;
import ru.t1.mayornikov.tm.model.Project;
import ru.t1.mayornikov.tm.util.TerminalUtil;

import java.util.Arrays;

public class ProjectStatusChangeByIdCommand extends AbstractProjectCommand{

    private static final String NAME = "project-status-change-by-id";

    private static final String DESCRIPTION = "Set status project by id.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PROJECT STATUS]");
        System.out.println("ENTER PROJECT ID:");
        final String userId = getUserId();
        final Project project = getProjectService().findOne(userId, TerminalUtil.nextLine());
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final Status status = Status.toStatus(TerminalUtil.nextLine());
        getProjectService().changeProjectStatus(project, status);
    }

}