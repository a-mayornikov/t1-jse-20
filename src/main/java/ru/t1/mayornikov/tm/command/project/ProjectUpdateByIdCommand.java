package ru.t1.mayornikov.tm.command.project;

import ru.t1.mayornikov.tm.model.Project;
import ru.t1.mayornikov.tm.util.TerminalUtil;

public class ProjectUpdateByIdCommand extends AbstractProjectCommand{

    private static final String NAME = "project-update-by-id";

    private static final String DESCRIPTION = "Update project by id.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String userId = getUserId();
        final Project project = getProjectService().findOne(userId, TerminalUtil.nextLine());
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        project.setName(name);
        project.setDescription(description);
    }

}