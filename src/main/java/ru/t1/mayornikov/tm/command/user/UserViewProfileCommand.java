package ru.t1.mayornikov.tm.command.user;

import ru.t1.mayornikov.tm.enumerated.Role;
import ru.t1.mayornikov.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand{

    private final static String NAME = "user-profile";

    private final static String DESCRIPTION = "Show current user's profile.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        final User user = getAuthService().getUser();
        System.out.println("[USER PROFILE]");
        System.out.println("ID:" + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole());
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }
    
}