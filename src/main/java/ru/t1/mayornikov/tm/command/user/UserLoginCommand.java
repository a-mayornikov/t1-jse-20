package ru.t1.mayornikov.tm.command.user;

import ru.t1.mayornikov.tm.enumerated.Role;
import ru.t1.mayornikov.tm.util.TerminalUtil;

public final class UserLoginCommand extends AbstractUserCommand{

    private final static String NAME = "user-login";

    private final static String DESCRIPTION = "Login user.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        getAuthService().login(login, password);
    }

    @Override
    public Role[] getRoles() {
        return null;
    }
    
}
