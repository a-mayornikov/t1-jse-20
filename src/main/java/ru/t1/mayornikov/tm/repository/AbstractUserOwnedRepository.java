package ru.t1.mayornikov.tm.repository;

import ru.t1.mayornikov.tm.api.repository.IUserOwnedRepository;
import ru.t1.mayornikov.tm.enumerated.Sort;
import ru.t1.mayornikov.tm.model.AbstractUserOwnedModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @Override
    public M add(final String userId, final M model) {
        if (userId == null) return null;
        model.setUserId(userId);
        return add(model);
    }

    @Override
    public void clear(final String userId) {
        if (userId == null) return;
        final List<M> models = findAll(userId);
        removeAll(models);
    }

    @Override
    public List<M> findAll(final String userId) {
        if (userId == null) return Collections.emptyList();
        final List<M> result = new ArrayList<>();
        for (final M m : models) if (userId.equals(m.getUserId())) result.add(m);
        return result;
    }

    @Override
    public List<M> findAll(final String userId, Comparator<M> comparator) {
        final List<M> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @Override
    public List<M> findAll(final String userId, Sort sort) {
        final List<M> result = findAll(userId);
        result.sort((Comparator<? super M>) sort.getComparator());
        return result;
    }

    @Override
    public M findOne(final String userId, final String id) {
        if (userId == null || id == null) return null;
        for (final M model: findAll(userId)) {
            if (!id.equals(model.getId())) continue;
            if (!userId.equals(model.getUserId())) continue;
            return model;
        }
        return null;
    }

    @Override
    public M findOne(final String userId, final Integer index) {
        return findAll(userId).get(index);
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        return findOne(userId, id) != null;
    }

    @Override
    public M remove(final String userId, final String id) {
        if (userId == null) return null;
        final M model = findOne(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public M remove(final String userId, final Integer index) {
        if (userId == null) return null;
        final M model = findOne(userId, index);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public Integer getSize(final String userId) {
        int count = 0;
        for (final M m : models) if (userId.equals(m.getUserId())) count++;
        return count;
    }

}