package ru.t1.mayornikov.tm.repository;

import ru.t1.mayornikov.tm.api.repository.IRepository;
import ru.t1.mayornikov.tm.model.AbstractModel;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    protected final List<M> models = new ArrayList<>();

    @Override
    public M add(final M model) {
        models.add(model);
        return model;
    }

    @Override
    public void clear() {
        models.clear();
    }

    @Override
    public List<M> findAll() {
        return models;
    }

    @Override
    public List<M> findAll(Comparator<M> comparator) {
        final List<M> result = new ArrayList<>(models);
        result.sort(comparator);
        return result;
    }

    @Override
    public M findOne(final String id) {
        for (final M model: models) if (id.equals(model.getId())) return model;
        return null;
    }

    @Override
    public M findOne(final Integer index) {
        return models.get(index);
    }

    @Override
    public boolean existsById(final String id) {
        return findOne(id) != null;
    }

    @Override
    public M remove(final M model) {
        if (model == null) return null;
        models.remove(model);
        return model;
    }

    @Override
    public M remove(final String id) {
        final M model = findOne(id);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public M remove(final Integer index) {
        final M model = findOne(index);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public void removeAll(final List<M> models) {
        for (final M m : models) remove(m);
    }

    @Override
    public Integer getSize() {
        return models.size();
    }

}