package ru.t1.mayornikov.tm.api.repository;

import ru.t1.mayornikov.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    Project create(String name, String description);

    Project create(String name);

}