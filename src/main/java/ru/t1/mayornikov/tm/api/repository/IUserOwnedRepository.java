package ru.t1.mayornikov.tm.api.repository;

import ru.t1.mayornikov.tm.enumerated.Sort;
import ru.t1.mayornikov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    M add(String userId, M model);

    void clear(String userId);

    List<M> findAll(String userId);

    List<M> findAll(String userId, Comparator<M> comparator);

    List<M> findAll(String userId, Sort sort);

    M findOne(String userId, String id);

    M findOne(String userId, Integer index);

    boolean existsById(String userId, String id);

    M remove(String userId, String id);

    M remove(String userId, Integer index);

    Integer getSize(String userId);

}