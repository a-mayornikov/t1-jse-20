package ru.t1.mayornikov.tm.api.service;

import ru.t1.mayornikov.tm.api.repository.IProjectRepository;
import ru.t1.mayornikov.tm.enumerated.Status;
import ru.t1.mayornikov.tm.model.Project;

import java.util.List;

public interface IProjectService extends IService<Project>, IProjectRepository {

    Project changeProjectStatus(Project project, Status status);

    Project update(String id, String name, String description);

    Project update(Integer index, String name, String description);

    void renderProjects();

    void renderProjects(List<Project> projects);

    void showProject(Project project);

}
