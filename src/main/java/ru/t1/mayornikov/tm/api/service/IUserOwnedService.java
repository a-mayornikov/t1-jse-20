package ru.t1.mayornikov.tm.api.service;

import ru.t1.mayornikov.tm.api.repository.IUserOwnedRepository;
import ru.t1.mayornikov.tm.enumerated.Sort;
import ru.t1.mayornikov.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M>, IService<M> {

    List<M> findAll(String userId, Sort sort);

    void removeAll(String userId, List<M> models);

}