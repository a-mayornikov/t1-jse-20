package ru.t1.mayornikov.tm.api.repository;

import ru.t1.mayornikov.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    M add(M model);

    void clear();

    List<M> findAll();

    List<M> findAll(Comparator<M> comparator);

    M findOne(String id);

    M findOne(Integer index);

    boolean existsById(String id);

    M remove(M model);

    M remove(String id);

    M remove(Integer index);

    void removeAll(List<M> models);

    Integer getSize();

}