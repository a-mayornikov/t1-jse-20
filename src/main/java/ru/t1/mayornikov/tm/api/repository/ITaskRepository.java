package ru.t1.mayornikov.tm.api.repository;

import ru.t1.mayornikov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    List<Task> findAllByProjectId(String projectId);

    Task create(String name, String description);

    Task create(String name);

}