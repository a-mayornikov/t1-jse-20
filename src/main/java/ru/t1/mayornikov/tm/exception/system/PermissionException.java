package ru.t1.mayornikov.tm.exception.system;

public final class PermissionException extends AbstractSystemException{

    public PermissionException() {
        super("Permission denied...");
    }

}