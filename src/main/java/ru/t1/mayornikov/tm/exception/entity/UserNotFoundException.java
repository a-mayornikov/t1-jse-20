package ru.t1.mayornikov.tm.exception.entity;

public final class UserNotFoundException extends AbstractEntityException{

    public UserNotFoundException() {
        super("User not found...");
    }

}