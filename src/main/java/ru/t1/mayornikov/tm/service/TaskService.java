package ru.t1.mayornikov.tm.service;

import ru.t1.mayornikov.tm.api.repository.ITaskRepository;
import ru.t1.mayornikov.tm.api.service.ITaskService;
import ru.t1.mayornikov.tm.enumerated.Status;
import ru.t1.mayornikov.tm.exception.entity.TaskNotFoundException;
import ru.t1.mayornikov.tm.exception.field.*;
import ru.t1.mayornikov.tm.exception.system.PermissionException;
import ru.t1.mayornikov.tm.model.Task;

import java.util.Collections;
import java.util.List;

public class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(final ITaskRepository repository) {
        super(repository);
    }

    @Override
    public void showTask(final Task task) {
        if (task == null) throw new TaskNotFoundException();
        final String id = task.getId();
        final String projectId = task.getProjectId();
        final String name = task.getName();
        final String description = task.getDescription();
        final String status = Status.toName(task.getStatus());
        if (!"".equals(id)) System.out.println("ID: " + id);
        if (projectId != null) System.out.println("PROJECT ID: " + projectId);
        if (!"".equals(name)) System.out.println("NAME: " + name);
        if (!"".equals(description)) System.out.println("TO DO: " + description);
        if (!"".equals(status)) System.out.println("STATUS: " + status);
    }

    @Override
    public void renderTasks(final List<Task> tasks) {
        int index = 1;
        for (Task task : tasks) {
            if (task == null) continue;
            System.out.println(index++ + ".");
            showTask(task);
        }
        if (index == 1) System.out.println("No one task found... ");
    }

    @Override
    public void renderTasks() {
        int index = 1;
        for(final Task task : findAll()) {
            if (task == null) continue;
            System.out.println(index++ + ".");
            showTask(task);
        }
        if (index == 1) System.out.println("No one task found... ");
    }

    @Override
    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        else if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return repository.create(name, description);
    }

    @Override
    public Task create(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return repository.create(name);
    }

    @Override
    public List<Task> findAllByProjectId(String projectId) {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return repository.findAllByProjectId(projectId);
    }

    @Override
    public Task update(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = findOne(id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task update(final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new IndexEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = findOne(index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task changeTaskStatus(final Task task, final Status status) {
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

}