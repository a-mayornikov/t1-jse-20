package ru.t1.mayornikov.tm.service;

import ru.t1.mayornikov.tm.api.repository.IUserOwnedRepository;
import ru.t1.mayornikov.tm.api.service.IUserOwnedService;
import ru.t1.mayornikov.tm.enumerated.Sort;
import ru.t1.mayornikov.tm.exception.entity.UserNotFoundException;
import ru.t1.mayornikov.tm.exception.field.IdEmptyException;
import ru.t1.mayornikov.tm.exception.field.IndexEmptyException;
import ru.t1.mayornikov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>> extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(final R repository) {
        super(repository);
    }

    @Override
    public M add(final String userId, final M model) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        if (model == null) return null;
        return repository.add(userId, model);
    }

    @Override
    public void clear(final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        repository.clear(userId);
    }

    @Override
    public List<M> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        return repository.findAll(userId);
    }

    @Override
    public List<M> findAll(final String userId, final Comparator<M> comparator) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        if (comparator == null) findAll();
        return repository.findAll(userId, comparator);
    }

    @Override
    public List<M> findAll(final String userId, final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        if (sort == null) findAll(userId);
        return repository.findAll(userId, (Comparator<M>) sort.getComparator());
    }

    @Override
    public M findOne(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        if (id.isEmpty()) throw new IdEmptyException();
        return repository.findOne(userId, id);
    }

    @Override
    public M findOne(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        if (index == null) throw new IndexEmptyException();
        return repository.findOne(userId, index);
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        if (id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(userId, id);
    }

    @Override
    public M remove(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        if (id.isEmpty()) throw new IdEmptyException();
        return repository.remove(userId, id);
    }

    @Override
    public M remove(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        if (index == null) throw new IndexEmptyException();
        return repository.remove(userId, index);
    }

    @Override
    public void removeAll(final String userId, List<M> models) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        if (models == null) return;
        repository.removeAll(models);
    }

    @Override
    public Integer getSize(final String userId) {
        return repository.getSize(userId);
    }

}